package br.com.teste;

/**
 * Classe com o algoritmo de resolução da questão 1.
 * @author Alexandre Hideki
 */
public class Main {

	/**
	 * Dada uma stream recupera a primeira vogal que sucede uma consoante e que também é unica na stream.
	 * @param stream de string
	 * @return - vogal e zero (0) caso não atenda a condição
	 */
	public char findFirstVogal(Stream stream){
	
	    char[] usedVogals = new char[5];
		char[] findedUniqueVogal = new char[5];
		
		int findedCount = 0;
	    char previousChar = 0;
		while(stream.hasNext()){
			
			char next = stream.getNext();
			if(previousChar == 0){
				alreadyFindedVogal(next, usedVogals);
				previousChar = next;
				continue;
			}

			if(!isVogal(previousChar) && isVogal(next)){
				if(!alreadyFindedVogal(next, usedVogals)){
					findedUniqueVogal[findedCount++] = next; 
				} else {
					if(reorganizer(next, findedUniqueVogal)) findedCount--;
				}
				continue;
			} 
			previousChar = next;
		}	
		
		return findedUniqueVogal[0];
	}
	
	/**
	 * Elimina o primeiro valor do array e reorganiza o array
	 * @param next - valor que será verificado se de fato se encontra na lista de vogais unicos encontrados
	 * @param findedUniqueVogal - lista com vogais unicos ne ordem em que foram encontrados
	 * @return true se a reorganização da lista ocorreu
	 */
	private boolean reorganizer(char next, char[] findedUniqueVogal) {
		for(int i=0; i< findedUniqueVogal.length; i++){
			try{
				if(findedUniqueVogal[i] == next){
					findedUniqueVogal[i] = findedUniqueVogal[i+1];
					return true;
				} 
			} catch(ArrayIndexOutOfBoundsException e){}
		}
		return false;
	}

	/**
	 * Verifica se valor recebido é uma vogal
	 * @param c - valor a verificar se é ou não uma vogal
	 * @return retorna true se valor é uma vogal
	 */
	private boolean isVogal(char c){
		char[] vogals = new char[]{'a', 'e', 'i', 'o', 'u'};
		for(int i=0; i<vogals.length; i++){
			if(vogals[i] == c){
				return true;
			}
		}
		return false;
	}
	
	private int cont = 0;
	
	/**
	 * Verifica se dada vogal ja foi encontrada anteriormente
	 * @param c - vogal que será verificada.
	 * @param usedVogals - lista com vogais ja verificadas
	 * @return - true se vogal ja foi verificado.
	 */
	private boolean alreadyFindedVogal(char c, char[] usedVogals){
		for(int i=0; i < usedVogals.length; i++){
			if(usedVogals[i] == c){
				return true;
			}
		}
		usedVogals[cont++] = c;
		return false;
	}
}