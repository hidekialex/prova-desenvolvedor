package br.com.teste;

/**
 * @author Alexandre Hideki
 */
public interface Stream {

    public char getNext();
    public boolean hasNext();
}