package br.com.teste;

/**
 * @author Alexandre Hideki
 */
public class StringStream implements Stream {

	private char[] stream;
	private int cont;
	private char nextChar;
	
	public StringStream(String value){
		this.stream = value.toCharArray();
	}
	
	public char getNext() {
		return nextChar;
	}

	public boolean hasNext() {
		try{
			nextChar = stream[cont++];
			return true;
		} catch(ArrayIndexOutOfBoundsException e){
			return false;
		}
	}
}
