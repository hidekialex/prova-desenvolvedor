package br.com.teste;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MainTests {

	/**
	 * Esperado a (primeira vogal unica na stream)
	 */
	@Test
	public void testCase1(){
		char finded = new Main().findFirstVogal(new StringStream("cage"));
		assertEquals('a', finded);
	}
	
	/**
	 * Esperado 'e'. A letra 'a' ja se repete antes na stream
	 */
	@Test
	public void testCase2(){
		char finded = new Main().findFirstVogal(new StringStream("acage"));
		assertEquals('e', finded);
	}
	
	/**
	 * Esperado 'a'. Inicia com 'e' porem o mesmo não é sucedido de uma consoantes.
	 * 'a' permanece como unica vogal na stream
	 */
	@Test
	public void testCase3(){
		char finded = new Main().findFirstVogal(new StringStream("ecage"));
		assertEquals('a', finded);
	}
	
	/**
	 * Esperado 'a'. 'i' tambem é unico, mas por 2 motivos não é o escolhido :
	 * não é o primeiro vogal da stream unica, e também não sucede consoante
	 */
	@Test
	public void testCase4(){
		char finded = new Main().findFirstVogal(new StringStream("ecagei"));
		assertEquals('a', finded);
	}
	
	/**
	 * Esperado 'a'. 'i' tambem é unico, mas apesar de suceder consoante não é
	 * a primeira vogal da stream. 
	 */
	@Test
	public void testCase5(){
		char finded = new Main().findFirstVogal(new StringStream("ecageci"));
		assertEquals('a', finded);
	}
	
	/**
	 * Esperado 'o'. uma sequencia de consoantes sucedidas de vogais, porem, a vogal unica nesta condição é a letra 'o' 
	 */
	@Test
	public void testCase6(){
		char finded = new Main().findFirstVogal(new StringStream("ecegecicico"));
		assertEquals('o', finded);
	}
	
	/**
	 * Esperado 'u'. uma sequencia de consoantes sucedidas de vogais, porem, a vogal unica nesta condição é a letra 'o' 
	 */
	@Test
	public void testCase7(){
		char finded = new Main().findFirstVogal(new StringStream("ecegecicicococicu"));
		assertEquals('u', finded);
	}
	
	/**
	 * Esperado 'e'. Teste do exemplo do exercicio 
	 */
	@Test
	public void testCase8(){
		char finded = new Main().findFirstVogal(new StringStream("aAbBABacfe"));
		assertEquals('e', finded);
	}
}
