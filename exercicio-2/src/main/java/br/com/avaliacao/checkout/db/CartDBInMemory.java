package br.com.avaliacao.checkout.db;

import br.com.avaliacao.checkout.model.Cart;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Repository
public class CartDBInMemory {

    private static Map<String, Cart> carts = new HashMap<>();

    public Cart save(Cart cart) {
        carts.put(cart.getCartId(), cart);
        return cart;
    }

    public Optional<Cart> findOne(String id) {
        return Optional.ofNullable(carts.get(id));
    }

    public void clear() {
        carts.clear();
    }
}
