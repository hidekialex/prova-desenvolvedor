package br.com.avaliacao.checkout.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class ConflictException extends Exception {

	private static final long serialVersionUID = -4533106445491552518L;

	public ConflictException(){}

	public ConflictException(String message) {
		super(message);
	}
}