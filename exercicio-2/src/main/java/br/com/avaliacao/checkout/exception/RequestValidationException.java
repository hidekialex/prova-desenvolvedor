package br.com.avaliacao.checkout.exception;

import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class RequestValidationException extends Exception {

	private static final long serialVersionUID = -3492572570692661554L;

	public RequestValidationException(BindingResult result) {
		super(buildErrorMessage(result));
	}
	
	private static String buildErrorMessage(BindingResult result) {
		StringBuilder errorsStr = new StringBuilder();
		for (FieldError error : result.getFieldErrors()) {
			errorsStr.append("[");
			errorsStr.append(error.getField());
			errorsStr.append(":");
			errorsStr.append(error.getDefaultMessage());
			errorsStr.append("] ");
		}
		
		for (ObjectError error : result.getGlobalErrors()) {
			errorsStr.append("[");
			errorsStr.append(error.getObjectName());
			errorsStr.append(":");
			errorsStr.append(error.getDefaultMessage());
			errorsStr.append("] ");
		}

		return errorsStr.toString().trim();
	}
}