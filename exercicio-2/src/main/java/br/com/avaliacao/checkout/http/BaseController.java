package br.com.avaliacao.checkout.http;

import org.springframework.validation.BindingResult;

import br.com.avaliacao.checkout.exception.RequestValidationException;

public class BaseController {

	public void validate(BindingResult result) throws RequestValidationException {
		if (result.hasErrors()) {
			throw new RequestValidationException(result);
		}
	}
}
