package br.com.avaliacao.checkout.http;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.avaliacao.checkout.http.request.ProductRequest;
import br.com.avaliacao.checkout.http.response.MessageResponse;
import br.com.avaliacao.checkout.services.CartService;

@RestController
@RequestMapping("/cart")
public class CartController extends BaseController {

    @Autowired
    private CartService cartService;
    
    @RequestMapping(value = "/adicionar", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public MessageResponse addToCard(@RequestBody @Valid ProductRequest request, final BindingResult result) throws Exception {
    	validate(result);
    	return cartService.addToCart(request);
    }
}

