package br.com.avaliacao.checkout.http.request;

import java.math.BigDecimal;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

public class ProductRequest {

	@NotBlank
	private String cartId;

	@NotNull
	private Integer q;

	@NotBlank
	private String codeProduct;

	@NotBlank	
	private String nameProduct;

	@NotBlank
	private String brand;

	@NotNull
	private BigDecimal price;

	public String getCartId() {
		return cartId;
	}

	public Integer getQ() {
		return q;
	}
	
	public void setQ(Integer q) {
		this.q = q;
	}
	
	public void setCartId(String cartId) {
		this.cartId = cartId;
	}

	public String getCodeProduct() {
		return codeProduct;
	}

	public void setCodeProduct(String codeProduct) {
		this.codeProduct = codeProduct;
	}

	public String getNameProduct() {
		return nameProduct;
	}

	public void setNameProduct(String nameProduct) {
		this.nameProduct = nameProduct;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}
}