package br.com.avaliacao.checkout.model;

public class CartItem {

    private Produto produto;

    private Integer quantity;

    public CartItem(){}
    
    public CartItem(Produto p, Integer q) {
		produto = p;
		quantity = q;
	}

	public Double getPrice() {
        return quantity * produto.getPreco().doubleValue();
    }

    //?
    public void incrementQuantity(final Integer quantity) {
        this.quantity += quantity;
    }

    public Produto getProduto() {
        return this.produto;
    }

    public Integer getQuantity() {
        return this.quantity;
    }

    public void setProduto(final Produto produto) {
        this.produto = produto;
    }

    public void setQuantity(final Integer quantity) {
        this.quantity = quantity;
    }

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CartItem other = (CartItem) obj;
		if (produto == null) {
			if (other.produto != null)
				return false;
		} else if (!produto.equals(other.produto))
			return false;
		if (quantity == null) {
			if (other.quantity != null)
				return false;
		} 
		return true;
	}    
}