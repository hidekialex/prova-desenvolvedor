package br.com.avaliacao.checkout.model;

import java.math.BigDecimal;

import br.com.avaliacao.checkout.http.request.ProductRequest;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

public class Produto {

    private String codigo;
    private String nome;
    private String marca;
    private BigDecimal preco;

	public Produto(){}
	
    public Produto(ProductRequest request) {
    	this.codigo = request.getCodeProduct();
    	this.nome = request.getNameProduct();
    	this.marca = request.getBrand();
    	this.preco = request.getPrice();
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}
	
	public BigDecimal getPreco() {
		return preco;
	}
	
	public void setPreco(BigDecimal preco) {
		this.preco = preco;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Produto other = (Produto) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		if (marca == null) {
			if (other.marca != null)
				return false;
		} else if (!marca.equals(other.marca))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (preco == null) {
			if (other.preco != null)
				return false;
		} else if (!preco.equals(other.preco))
			return false;
		return true;
	}
}