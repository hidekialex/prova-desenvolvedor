package br.com.avaliacao.checkout.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.avaliacao.checkout.db.CartDBInMemory;
import br.com.avaliacao.checkout.exception.ConflictException;
import br.com.avaliacao.checkout.http.request.ProductRequest;
import br.com.avaliacao.checkout.http.response.MessageResponse;
import br.com.avaliacao.checkout.model.Cart;
import br.com.avaliacao.checkout.model.Cart.CartStatus;
import br.com.avaliacao.checkout.model.CartItem;
import br.com.avaliacao.checkout.model.Produto;

@Service
public class CartService {

	@Autowired
    private CartDBInMemory cartDB;
	
	public MessageResponse addToCart(ProductRequest request) throws ConflictException{
		
		Produto p = new Produto(request);
        CartItem item = new CartItem(p, request.getQ());
        
        Optional<Cart> cartOp = cartDB.findOne(request.getCartId());

        Cart cart = null;
        if(cartOp.isPresent()){
        	cart = cartOp.get();
        	if(CartStatus.OPENED.equals(cart.getStatus())){
        		if(cart.getItems().contains(item)){
        			int index = cart.getItems().indexOf(item);
        			cart.getItems().get(index).incrementQuantity(request.getQ());
        		} else {
        			cart.getItems().add(item);        			
        		}        		
        	} else if(CartStatus.ACCOMPLISHED.equals(cart.getStatus())){
        		throw new ConflictException("This cart status is ACCOMPLISHED.");
        	} else if(CartStatus.ABANDONED.equals(cart.getStatus())){
        		throw new ConflictException("This cart status is ABANDONED.");
        	}
        } else {
        	cart = new Cart();
        	cart.setCartId(request.getCartId());
        	cart.getItems().add(item);     
        }       
        cartDB.save(cart);
        return new MessageResponse("Item inserted to Cart with success.");
	}
}
