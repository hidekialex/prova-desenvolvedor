package br.com.avaliacao.checkout.http;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.avaliacao.checkout.http.request.ProductRequest;
import br.com.avaliacao.checkout.http.response.MessageResponse;
import br.com.avaliacao.checkout.services.CartService;

public class CartControllerTests {

	@InjectMocks
	private CartController cartController;
	
	@Mock
	private CartService cartService;
	
	@Captor
	private ArgumentCaptor<ProductRequest> productRequestCaptor;
	
	private MockMvc mockMvc;
	
	private ObjectMapper objectMapper = new ObjectMapper();
	
	private static final String PATH_CART_ADICIONAR = "/cart/adicionar";
	
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(cartController).build();
	}
	
	@Test
	public void enviandoCartIdNullNaRequest() throws Exception{
		
		ProductRequest request = new ProductRequest();
		request.setCartId(null);
		request.setQ(1);
		request.setCodeProduct("codeProduct");
		request.setNameProduct("nameProduct");
		request.setBrand("brand");
		request.setPrice(new BigDecimal("100.00"));
		
		String message = mockMvc.perform(post(PATH_CART_ADICIONAR).content(objectMapper.writeValueAsString(request)).header("Content-Type", "application/json"))
				.andExpect(status().isBadRequest()).andReturn().getResolvedException().getMessage();
		
		assertEquals("[cartId:may not be empty]", message);
	}
	
	@Test
	public void enviandoCartIdEmBrancoNaRequest() throws Exception {
		
		ProductRequest request = new ProductRequest();
		request.setCartId("");
		request.setQ(1);
		request.setCodeProduct("codeProduct");
		request.setNameProduct("nameProduct");
		request.setBrand("brand");
		request.setPrice(new BigDecimal("100.00"));
		
		String message = mockMvc.perform(post(PATH_CART_ADICIONAR).content(objectMapper.writeValueAsString(request)).header("Content-Type", "application/json"))
				.andExpect(status().isBadRequest()).andReturn().getResolvedException().getMessage();
		
		assertEquals("[cartId:may not be empty]", message);
	}
	
	@Test
	public void enviandoQNullNaRequest() throws Exception {
		
		ProductRequest request = new ProductRequest();
		request.setCartId("cartId");
		request.setQ(null);
		request.setCodeProduct("codeProduct");
		request.setNameProduct("nameProduct");
		request.setBrand("brand");
		request.setPrice(new BigDecimal("100.00"));
		
		String message = mockMvc.perform(post(PATH_CART_ADICIONAR).content(objectMapper.writeValueAsString(request)).header("Content-Type", "application/json"))
				.andExpect(status().isBadRequest()).andReturn().getResolvedException().getMessage();
		
		assertEquals("[q:may not be null]", message);
	}
	
	@Test
	public void enviandoCodeProductNullNaRequest() throws Exception {
		
		ProductRequest request = new ProductRequest();
		request.setCartId("cartId");
		request.setQ(1);
		request.setCodeProduct(null);
		request.setNameProduct("nameProduct");
		request.setBrand("brand");
		request.setPrice(new BigDecimal("100.00"));
		
		String message = mockMvc.perform(post(PATH_CART_ADICIONAR).content(objectMapper.writeValueAsString(request)).header("Content-Type", "application/json"))
				.andExpect(status().isBadRequest()).andReturn().getResolvedException().getMessage();
		
		assertEquals("[codeProduct:may not be empty]", message);
	}
	
	@Test
	public void enviandoCodeProductEmBrancoNaRequest() throws Exception {
			
		ProductRequest request = new ProductRequest();
		request.setCartId("cartId");
		request.setQ(1);
		request.setCodeProduct("");
		request.setNameProduct("nameProduct");
		request.setBrand("brand");
		request.setPrice(new BigDecimal("100.00"));
		
		String message = mockMvc.perform(post(PATH_CART_ADICIONAR).content(objectMapper.writeValueAsString(request)).header("Content-Type", "application/json"))
				.andExpect(status().isBadRequest()).andReturn().getResolvedException().getMessage();
		
		assertEquals("[codeProduct:may not be empty]", message);
	}
	
	@Test
	public void enviandoNameProductNullNaRequest() throws Exception {
		
		ProductRequest request = new ProductRequest();
		request.setCartId("cartId");
		request.setQ(1);
		request.setCodeProduct("codeProduct");
		request.setNameProduct(null);
		request.setBrand("brand");
		request.setPrice(new BigDecimal("100.00"));
		
		String message = mockMvc.perform(post(PATH_CART_ADICIONAR).content(objectMapper.writeValueAsString(request)).header("Content-Type", "application/json"))
				.andExpect(status().isBadRequest()).andReturn().getResolvedException().getMessage();
		
		assertEquals("[nameProduct:may not be empty]", message);
	}
	
	@Test
	public void enviandoNameProductEmBrancoNaRequest() throws Exception {
			
		ProductRequest request = new ProductRequest();
		request.setCartId("cartId");
		request.setQ(1);
		request.setCodeProduct("codeProduct");
		request.setNameProduct("");
		request.setBrand("brand");
		request.setPrice(new BigDecimal("100.00"));
		
		String message = mockMvc.perform(post(PATH_CART_ADICIONAR).content(objectMapper.writeValueAsString(request)).header("Content-Type", "application/json"))
				.andExpect(status().isBadRequest()).andReturn().getResolvedException().getMessage();
		
		assertEquals("[nameProduct:may not be empty]", message);
	}
	
	@Test
	public void enviandoBrandNullNaRequest() throws Exception {
		
		ProductRequest request = new ProductRequest();
		request.setCartId("cartId");
		request.setQ(1);
		request.setCodeProduct("codeProduct");
		request.setNameProduct("nameProduct");
		request.setBrand(null);
		request.setPrice(new BigDecimal("100.00"));
		
		String message = mockMvc.perform(post(PATH_CART_ADICIONAR).content(objectMapper.writeValueAsString(request)).header("Content-Type", "application/json"))
				.andExpect(status().isBadRequest()).andReturn().getResolvedException().getMessage();
		
		assertEquals("[brand:may not be empty]", message);
	}
	
	@Test
	public void enviandoBrandEmBrancoNaRequest() throws Exception {
		
		ProductRequest request = new ProductRequest();
		request.setCartId("cartId");
		request.setQ(1);
		request.setCodeProduct("codeProduct");
		request.setNameProduct("nameProduct");
		request.setBrand("");
		request.setPrice(new BigDecimal("100.00"));
		
		String message = mockMvc.perform(post(PATH_CART_ADICIONAR).content(objectMapper.writeValueAsString(request)).header("Content-Type", "application/json"))
				.andExpect(status().isBadRequest()).andReturn().getResolvedException().getMessage();
		
		assertEquals("[brand:may not be empty]", message);
	}
	
	@Test
	public void enviandoPriceNullNaRequest() throws Exception {
		
		ProductRequest request = new ProductRequest();
		request.setCartId("cartId");
		request.setQ(1);
		request.setCodeProduct("codeProduct");
		request.setNameProduct("nameProduct");
		request.setBrand("brand");
		request.setPrice(null);
		
		String message = mockMvc.perform(post(PATH_CART_ADICIONAR).content(objectMapper.writeValueAsString(request)).header("Content-Type", "application/json"))
				.andExpect(status().isBadRequest()).andReturn().getResolvedException().getMessage();

		assertEquals("[price:may not be null]", message);
	}
	
	@Test
	public void naoEnviandoNenhumAtributoNaRequest() throws Exception {
		
		ProductRequest request = new ProductRequest();
		
		String message = mockMvc.perform(post(PATH_CART_ADICIONAR).content(objectMapper.writeValueAsString(request)).header("Content-Type", "application/json"))
				.andExpect(status().isBadRequest()).andReturn().getResolvedException().getMessage();

		assertTrue(message.contains("[brand:may not be empty]"));
		assertTrue(message.contains("[codeProduct:may not be empty]"));
		assertTrue(message.contains("[cartId:may not be empty]"));
		assertTrue(message.contains("[nameProduct:may not be empty]"));
		assertTrue(message.contains("[q:may not be null]"));
		assertTrue(message.contains("[price:may not be null]"));
	}
	
	@Test
	public void enviandoTodasAsInformacoesCorretamenteNaRequest() throws Exception {
		
		ProductRequest request = new ProductRequest();
		request.setCartId("cartId");
		request.setQ(1);
		request.setCodeProduct("codeProduct");
		request.setNameProduct("nameProduct");
		request.setBrand("brand");
		request.setPrice(new BigDecimal("100.00"));
		
		when(cartService.addToCart(any(ProductRequest.class))).thenReturn(new MessageResponse("Item inserted to Cart with success."));
				
		mockMvc.perform(post(PATH_CART_ADICIONAR).content(objectMapper.writeValueAsString(request)).header("Content-Type", "application/json"))
				.andExpect(status().isCreated())
				.andExpect(jsonPath("$.message", is("Item inserted to Cart with success.")));
		
		verify(cartService, times(1)).addToCart(productRequestCaptor.capture());
		
		ProductRequest captured = productRequestCaptor.getValue();
		
		assertEquals(request.getBrand(), captured.getBrand());
		assertEquals(request.getCartId(), captured.getCartId());
		assertEquals(request.getCodeProduct(), captured.getCodeProduct());
		assertEquals(request.getNameProduct(), captured.getNameProduct());
		assertEquals(request.getPrice(), captured.getPrice());
		assertEquals(request.getQ(), captured.getQ());
	}
}