package br.com.avaliacao.checkout.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import br.com.avaliacao.checkout.db.CartDBInMemory;
import br.com.avaliacao.checkout.exception.ConflictException;
import br.com.avaliacao.checkout.http.request.ProductRequest;
import br.com.avaliacao.checkout.http.response.MessageResponse;
import br.com.avaliacao.checkout.model.Cart;
import br.com.avaliacao.checkout.model.Cart.CartStatus;
import br.com.avaliacao.checkout.model.CartItem;
import br.com.avaliacao.checkout.model.Produto;

public class CartServiceTests {

	@InjectMocks
	private CartService cartService;
	
	@Mock
	private CartDBInMemory cartDbInMemory;
	
	@Captor
	private ArgumentCaptor<Cart> cartCaptor;
	
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void adicionandoItemNoCarrinhoExistente() throws ConflictException{
		
		ProductRequest request = new ProductRequest();
		
		Cart cart = new Cart();
		cart.setStatus(CartStatus.OPENED);
		
		when(cartDbInMemory.findOne(eq(request.getCartId()))).thenReturn(Optional.of(cart));
		
		MessageResponse response = cartService.addToCart(request);
		
		assertEquals("Item inserted to Cart with success.", response.getMessage());
		
		verify(cartDbInMemory, times(1)).findOne(eq(request.getCartId()));
		verify(cartDbInMemory, times(1)).save(cartCaptor.capture());
		
		Cart captured = cartCaptor.getValue();
		
		assertTrue(captured.getItems().size() == 1);		
	}
	
	@Test
	public void adicionandoItemNoCarrinhoNaoExistente() throws ConflictException {
		
		ProductRequest request = new ProductRequest();
		request.setCartId("cartId");
		
		Cart cart = new Cart();
		cart.setStatus(CartStatus.OPENED);
		
		when(cartDbInMemory.findOne(eq(request.getCartId()))).thenReturn(Optional.ofNullable(null));
		
		MessageResponse response = cartService.addToCart(request);
		
		assertEquals("Item inserted to Cart with success.", response.getMessage());
		
		verify(cartDbInMemory, times(1)).findOne(eq(request.getCartId()));
		verify(cartDbInMemory, times(1)).save(cartCaptor.capture());
		
		Cart captured = cartCaptor.getValue();
		
		assertTrue(captured.getItems().size() == 1);
		assertEquals(request.getCartId(), captured.getCartId());
	}
	
	@Test
	public void adicionandoItensRepetidosNoCarrinho() throws ConflictException {
		
		ProductRequest request = new ProductRequest();
		request.setCartId("cartId");
		request.setBrand("brand");
		request.setCodeProduct("codeProduct");
		request.setNameProduct("nameProduct");
		request.setPrice(new BigDecimal("10.00"));
		request.setQ(2);
		
		Cart cart = new Cart();
		cart.setCartId("cartId");
		cart.setStatus(CartStatus.OPENED);
		
		CartItem cartItem = new CartItem();
		Produto produto = new Produto(request);
		cartItem.setProduto(produto);
		cartItem.setQuantity(1);
		
		ArrayList<CartItem> itens = new ArrayList<>();
		itens.add(cartItem);
		
		cart.setItems(itens);
		
		when(cartDbInMemory.findOne(eq(request.getCartId()))).thenReturn(Optional.ofNullable(cart));
		
		MessageResponse response = cartService.addToCart(request);
		
		assertEquals("Item inserted to Cart with success.", response.getMessage());
		
		verify(cartDbInMemory, times(1)).findOne(eq(request.getCartId()));
		verify(cartDbInMemory, times(1)).save(cartCaptor.capture());
		
		Cart captured = cartCaptor.getValue();
		
		assertTrue(captured.getItems().size() == 1);
		assertTrue(captured.getItems().get(0).getQuantity() == 3);
		assertEquals(request.getCartId(), captured.getCartId());
	}
	
	@Test
	public void adicionandoItensNaoRepetidosNoCarrinho() throws Exception {
		
		ProductRequest request = new ProductRequest();
		request.setCartId("cartId");
		request.setBrand("brand");
		request.setCodeProduct("codeProduct");
		request.setNameProduct("nameProduct");
		request.setPrice(new BigDecimal("10.00"));
		request.setQ(3);
		
		Cart cart = new Cart();
		cart.setCartId("cartId");
		cart.setStatus(CartStatus.OPENED);
		
		CartItem cartItem = new CartItem();
		Produto produto = new Produto();
		produto.setCodigo("codeProduct2");
		produto.setNome("nameProduct2");
		produto.setMarca("brand2");
		
		cartItem.setProduto(produto);
		cartItem.setQuantity(1);
		
		ArrayList<CartItem> itens = new ArrayList<>();
		itens.add(cartItem);
		
		cart.setItems(itens);
		
		when(cartDbInMemory.findOne(eq(request.getCartId()))).thenReturn(Optional.ofNullable(cart));
		
		MessageResponse response = cartService.addToCart(request);
		
		assertEquals("Item inserted to Cart with success.", response.getMessage());
		
		verify(cartDbInMemory, times(1)).findOne(eq(request.getCartId()));
		verify(cartDbInMemory, times(1)).save(cartCaptor.capture());
		
		Cart captured = cartCaptor.getValue();
		
		assertTrue(captured.getItems().size() == 2);
		assertTrue(captured.getItems().get(0).getQuantity() == 1);
		assertTrue(captured.getItems().get(1).getQuantity() == 3);
		assertEquals(request.getCartId(), captured.getCartId());
	}
	
	@Test
	public void tentandoAdicionarItemEmCarrinhoComStatusAccomplished() {
		
		ProductRequest request = new ProductRequest();
		request.setCartId("cartId");
		
		Cart cart = new Cart();
		cart.setCartId("cartId");
		cart.setStatus(CartStatus.ACCOMPLISHED);
		
		when(cartDbInMemory.findOne(eq(request.getCartId()))).thenReturn(Optional.ofNullable(cart));
		
		try {
			cartService.addToCart(request);
		} catch (ConflictException e) {
			assertEquals("This cart status is ACCOMPLISHED.", e.getMessage());
		}
		
		verify(cartDbInMemory, times(1)).findOne(eq(request.getCartId()));
		verify(cartDbInMemory, times(0)).save(any(Cart.class));
	}
	
	@Test
	public void tentandoAdicionarItemEmCarrinhoComStatusAbandoned(){
		
		ProductRequest request = new ProductRequest();
		request.setCartId("cartId");
		
		Cart cart = new Cart();
		cart.setCartId("cartId");
		cart.setStatus(CartStatus.ABANDONED);
		
		when(cartDbInMemory.findOne(eq(request.getCartId()))).thenReturn(Optional.ofNullable(cart));
		
		try {
			cartService.addToCart(request);
		} catch (ConflictException e) {
			assertEquals("This cart status is ABANDONED.", e.getMessage());
		}
		
		verify(cartDbInMemory, times(1)).findOne(eq(request.getCartId()));
		verify(cartDbInMemory, times(0)).save(any(Cart.class));
	}	
}